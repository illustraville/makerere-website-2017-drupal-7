<?php
	// Add some cool text to the search block form
	function mak_theme_d7_form_alter(&$form, &$form_state, $form_id) {
    if ($form_id == 'search_block_form') {
      // HTML5 placeholder attribute
      $form['search_block_form']['#attributes']['placeholder'] = t('Enter any search terms, hit Enter..');
    }

  }

  function mak_theme_d7_preprocess_page(&$vars) {
      // - page--example.tpl.php  

    if (isset($vars['node'])) {
    $vars['theme_hook_suggestion'] = 'page__'.$vars['node']->type; //
    }
  }






?>
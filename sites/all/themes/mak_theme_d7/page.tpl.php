<?php

/**
 * @file
 * Copied from Bartik's theme implementation to display a single Drupal page in Makerere Template.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['topmenu']: Items for the topmenu.
 * - $page['socialicons']: Items for the Social Icon in the top region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['mainslider']: Items for the Main Slider.
 * - $page['position01']: Items for the first position.
 * - $page['position02']: Items for the second position.
 * - $page['position03']: Items for the third position.
 * - $page['mainmenu']: Items for the Main Menu.
 * - $page['loginblock']: Items for the Log in Block position.
 * - $page['search']: Items for the search position.
 * - $page['footer_01']: Items for the first footer column.
 * - $page['footer_02']: Items for the second footer column.
 * - $page['footer_03']: Items for the third footer column.
 * - $page['footer_04']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see html.tpl.php
 */
?>
<!-- Wrapper -->

<div id="wrapper"> 
  
  <!-- TOPBAR -->
  <div id="topbar" class="">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 hidden-xs"> <?php print render($page['topmenu']); ?> </div>
        <div class="col-sm-6 hidden-xs">
          <div class="social-icons social-icons-colored-hover"> <?php print render($page['socialicons']); ?> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end: TOPBAR --> 
  
  <!-- Header -->
  <header id="header">
    <div id="header-wrap">
      <div class="container"> 
        <!--Logo-->
        <div id="logo"> <a href="<?php print $front_page; ?>" class="logo" data-dark-logo="images/mak-logo-01.png"> <img src="/<?php print $directory ?>/images/mak-logo-01.png" alt="Makerere University"> </a>
          <div class="sitetitle hidden-sm hidden-xs">
            <h1><?php print $site_name; ?></h1>
            <span><?php print $site_slogan; ?></span> </div>
        </div>
        <!--End: Logo--> 
        
        <!--Top Search Form-->
        <div id="top-search"> <?php print render($page['search']); ?> </div>
        <!--end: Top Search Form--> 
        
        <!--Header Extras-->
        <div class="header-extras">
          <ul>
            <li> 
              <!--top search--> 
              <a id="top-search-trigger" href="#" class="toggle-item"> <i class="fa fa-search"></i> <i class="fa fa-close"></i> </a> 
              <!--end: top search--> 
            </li>
            <li>
              <div class="topbar-dropdown"> <a class="title"><i class="fa fa-unlock"></i></a>
                <div class="dropdown-list">
                  <div class="content"> <?php print render($page['loginblock']); ?> </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <!--end: Header Extras--> 
        
        <!--Navigation Resposnive Trigger-->
        <div id="mainMenu-trigger">
          <button class="lines-button x"> <span class="lines"></span> </button>
        </div>
        <!--end: Navigation Resposnive Trigger--> 
        
        <!--Navigation-->
        <div id="mainMenu" class="light">
          <div class="container">
            <nav> <?php print render($page['mainmenu']); ?> </nav>
          </div>
        </div>
        <!--end: Navigation--> 
      </div>
    </div>
  </header>
  <!-- end: Header --> 
  
  <!-- Slider -->
  <?php if ($page['mainslider']): ?>
  <div>
    <div id="slider" class="inspiro-slider"> <?php print render($page['mainslider']); ?> </div>
  </div>
  <?php endif; ?>
  <!--end: Slider --> 
  
  <!-- TITLE -->
  <?php if ($title): ?>
  <section id="page-title">
    <div class="container">
      <div class="page-title">
        <h1><?php print $title; ?></h1>
        <div class="breadcrumb"><?php print $breadcrumb; ?></div>
      </div>
    </div>
  </section>
  <?php endif; ?>
  <!-- end: TITLE --> 
  
  <!-- Latest News -->
  <?php if ($page['position01']): ?>
  <section class="p-t-40 p-b-40">
    <div class="container"> <?php print render($page['position01']); ?> </div>
  </section>
  <?php endif; ?>
  <!-- end: LATEST NEWS --> 
  
  <!-- MASSAGE FROM PRINCIPAL -->
  <?php if ($page['position02']): ?>
  <section class="row background-grey testimonial testimonial-left">
    <div class="container"> <?php print render($page['position02']); ?> </div>
  </section>
  <?php endif; ?>
  <!-- end MESSAGE FROM PRINCIPAL --> 
  
  <!-- CONTENT MAIN -->
  <section class="p-t-60 p-b-50">
    <div class="container">
      <?php if ($tabs): ?>
      <div class="tabs"> <?php print render($tabs); ?> </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
      <ul class="action-links">
        <?php print render($action_links); ?>
      </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?> <?php print $feed_icons; ?> </div>
  </section>
  <!-- end CONTENT MAIN --> 
  
  <!-- Footer -->
  <footer id="footer" class="footer-light">
    <div class="footer-content">
      <div class="container">
        <div class="row">
          <div class="col-md-4"> 
            <!-- Footer widget area 1 -->
            <div class="widget clearfix widget-contact-us" style="background-image: url('/<?php print $directory ?>/images/world-map-dark.png'); background-position: 50% 20px; background-repeat: no-repeat"> <?php print render($page['footer01']); ?> </div>
            <!-- end: Footer widget area 1 --> 
          </div>
          <div class="col-md-2"> 
            <!-- Footer widget area 2 -->
            <div class="widget"> <?php print render($page['footer02']); ?> </div>
            <!-- end: Footer widget area 2 --> 
          </div>
          <div class="col-md-3"> 
            <!-- Footer widget area 3 -->
            <div class="widget"> <?php print render($page['footer03']); ?> </div>
            <!-- end: Footer widget area 3 --> 
          </div>
          <div class="col-md-3"> 
            <!-- Footer widget area 4 --> 
            <?php print render($page['footer04']); ?> 
            <!-- end: Footer widget area 4 --> 
          </div>
        </div>
      </div>
    </div>
    <div class="copyright-content">
      <div class="container">
        <div class="copyright-text text-center"><?php print render($page['footer']); ?></div>
      </div>
    </div>
  </footer>
  <!-- end: Footer --> 
  
</div>
<!-- end: Wrapper --> 

<!-- Go to top button --> 
<a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a> 
<!--Plugins--> 
<script src="/<?php print $directory ?>/js/jquery.js"></script> 
<script src="/<?php print $directory ?>/js/plugins.js"></script> 

<!--Template functions--> 
<script src="/<?php print $directory ?>/js/functions.js"></script> 

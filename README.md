# README #

This README documents whatever steps are necessary to get your website up and running.

### What is this repository for? ###

* Drupal 7 Universal Makerere University Unit Template
* Version 1.1
* Case Study in this Project : Makerere Peace & Conflict Studies Center (You can Change this)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Summary of set up
* All requirements needed for a Drupal 7 Set up (Drupal 7.56 version at the time of development. (Drupal 8 set up coming soon)
* You can use the Theme independent of the set up.
Dependencies (at the time of dev't)
* PHP 5.6.12
* MySQL 5.6.. 

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Installed Modules ###
token libraries pathauto ctools views admin_menu date metatag module_filter xmlsitemap media superfish panels views_slideshow calendar wysiwyg, file_entity, devel, Colorbox

(Can be updated via drush)

### Installed Libraries ###
jquery.cycle, supefish.js, ckeditor

### Who do I talk to? ###
* dwamala@dicts.mak.ac.ug

### Preview/Screenshot ###
![Screen-Shot-Template.jpg](Screen-Shot-Template.jpg)